import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, PopoverController, ModalController, AlertController} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import { TranslateService } from '@ngx-translate/core';
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
declare var google;

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})

export class HomePage {
    rootPage: any = 'LoginPage';
    tab1: string = 'LandingPage';
    tab2: string = 'FavouratePage';
    tab3: string = 'OrderPage';
    tab4: string = 'ProfilePage';
    location: any;
    myLat: any;
    myLong: any;
    selected_lang: any;
    user_first_name: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        public modalCtrl: ModalController,
        private geolocation: Geolocation,
        private alertCtrl: AlertController,
        public translate: TranslateService,
        protected http: Http


    ) {

        this.user_first_name=localStorage.getItem("user_first_name");

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }


    ionViewDidEnter() {
        this.geolocation.getCurrentPosition().then(pos => {

            this.myLat = pos.coords.latitude;
            this.myLong = pos.coords.longitude;


            let geocoder = new google.maps.Geocoder;
            let latlng = {lat: pos.coords.latitude, lng: pos.coords.longitude};
            geocoder.geocode({'location': latlng}, (results, status) => {
                // localStorage.setItem("city", results[0].address_components[0].long_name);
                localStorage.setItem("city", results[0].formatted_address);
                this.location = localStorage.getItem('city');
            });

        });


        if (localStorage.getItem("token") == null) {
            this.navCtrl.push('LoginPage')

        }
    }

    logout() {

        let alert = this.alertCtrl.create({
            title: this.get_trans("are_you_sure"),
            buttons: [
                {
                    text: this.get_trans("no"),
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.get_trans("yes"),
                    handler: () => {
                        let url = base_page.api_url() + 'api/v1/logout';

                        let postData = {
                            "token": localStorage.getItem("token"),
                            "user_id": localStorage.getItem("userid"),
                            "device_id": localStorage.getItem("device_id"),
                        };

                        let xhr_data: Observable<any>;

                        xhr_data = this.http.post(url, postData);
                        xhr_data.subscribe(data => {

                            localStorage.clear();
                            this.navCtrl.setRoot('LoginPage');

                        });
                    }
                }
            ]
        });

        alert.present();

    }

    public show_word(word){
        let alert = this.alertCtrl.create({
            title: this.get_trans("notification"),
            subTitle: word
        });
        alert.present();
    }

    presentPopover(myEvent) {
        let popover = this.popoverCtrl.create('UselocationPopoverPage');
        popover.present({
            ev: myEvent
        });
    }



    filter() {
        let modal = this.modalCtrl.create('FilterPage');
        modal.present();
    }


}
