import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams,ViewController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-technical-support',
    templateUrl: 'technical-support.html',
})
export class TechnicalSupportPage {

    public message:any;
    data: Observable<any>;
    newData: any = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private alertCtrl: AlertController,
        protected http: Http,
        public translate: TranslateService,
        public viewCtrl: ViewController,

    ) {
        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    save(){

        if(this.message.length==0){
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("all_fields_are_required")
            });
            alert.present();
            return;
        }

        var url = base_page.api_url() + 'api/v1/send_email_to_admins';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "message": this.message,
        }

        this.data= this.http.post(url, postData);
        this.data.subscribe(data => {

            this.newData=[(JSON.parse(data._body))];

            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans(this.newData[0].message)
            });
            alert.present();


        });

    }

}
