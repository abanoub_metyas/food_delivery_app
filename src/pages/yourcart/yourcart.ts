import {Component} from '@angular/core';
import {IonicPage, NavController, ModalController,AlertController,NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page'
import { TranslateService } from '@ngx-translate/core';


declare var $: any;
@IonicPage()
@Component({
    selector: 'page-yourcart',
    templateUrl: 'yourcart.html',
})
export class YourcartPage {
    data: Observable<any>;
    currentNumber: any;
    sub: any;
    delivery: any;
    total: any;
    descound: any ;
    allItems: any = [];
    items: any = [];
    cartItems: any = [];
    cartItems_quantity: any = [];
    restaurant_id: any;
    card: any = [];
    currency_name: any = "";

    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        protected http: Http,
        public translate: TranslateService,
        public alertCtrl: AlertController,
        public navParams: NavParams,

    ) {
        this.currentNumber = 1;

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }


    ionViewDidEnter() {
        this.sub=0;
        this.delivery=0;
        this.total=0;
        this.descound=0;

        this.restaurant_id=this.navParams.get('restaurant_id');

        this.card=localStorage.getItem(this.restaurant_id+"_cardList");

        if(this.card==null||this.card=='[]'){
            this.card=[];
        }

        if (this.card.length == 0) {
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("please_add_order_to_cart")
            });
            alert.present();

            this.navCtrl.pop();
            return;
        }


        var url = base_page.api_url() + 'api/v1/cartItems';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "items": localStorage.getItem(this.restaurant_id+"_cardList"),
            "restaurant_id": this.restaurant_id,
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.allItems=[(JSON.parse(data._body))];

            if (this.allItems[0].message == 1) {
                this.items=[(this.allItems[0].data)];

                this.descound = this.allItems[0].sub - this.allItems[0].sub_offer;

                this.cartItems_quantity = this.allItems[0].quantity;
                this.cartItems = this.allItems[0].quantity;
                this.sub = this.allItems[0].sub;
                this.delivery = this.allItems[0].delivery;
                this.total = this.sub + this.delivery - this.descound;
                this.currency_name=this.allItems[0].restaurant_obj.currency_name;

            }
            else {
                console.log(data);
            }
        });
    }

    increment(id, price, befor_price) {
        this.cartItems_quantity[id] = this.cartItems_quantity[id] + 1;
        this.update_cart_items();
    }

    decrement(id, price, befor_price) {
        if (this.cartItems_quantity[id] > 1) {
            this.cartItems_quantity[id] = this.cartItems_quantity[id] - 1;
            this.update_cart_items();
        }
    }

    remove_order(item_id){
        delete this.cartItems[item_id];
        this.update_cart_items();
        this.ionViewDidEnter();
    }

    update_cart_items() {
        this.cartItems = [];

        for (let key in this.cartItems_quantity) {
            this.cartItems[key] = [key, this.cartItems_quantity[key]];
        }

        localStorage.setItem(this.restaurant_id+'_cardList', JSON.stringify(this.cartItems));

        this.ionViewDidEnter();
    }

    back() {
        this.navCtrl.pop();
    }

    promo() {
        let modal = this.modalCtrl.create('PromoPage');
        modal.present();
    }

    checkout() {

        if (this.allItems[0].minimum_charge>(this.sub-this.descound)){
            let alert = this.alertCtrl.create({
                title:this.get_trans("notification"),
                subTitle: this.get_trans("order_total_less_than_restaurant_minimum_charge")
            });
            alert.present();

            return false;
        }

        this.navCtrl.push('DeliveryPage',{
            'restaurant_id':this.restaurant_id
        });
    }

}
