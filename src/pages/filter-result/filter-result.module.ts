import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {FilterResultPage} from './filter-result';
import {Ionic2RatingModule} from 'ionic2-rating';

@NgModule({
    declarations: [
        FilterResultPage,
    ],
    imports: [
        IonicPageModule.forChild(FilterResultPage),
        Ionic2RatingModule

    ],
})
export class FilterResultPageModule {
}
