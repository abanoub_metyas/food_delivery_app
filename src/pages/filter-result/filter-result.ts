import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams,ModalController,ViewController} from 'ionic-angular';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-filter-result',
    templateUrl: 'filter-result.html',
})
export class FilterResultPage {
    data: Observable<any>;
    items:any=[];
    restaurants:any=[];
    restaurants_count:any;
    public uploads_url=base_page.api_uploads_url();
    public selected_lang:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        protected http: Http,
        public translate: TranslateService

    ) {
        this.selected_lang=localStorage.getItem("selected_lang");
        if(this.selected_lang==null){
            this.selected_lang="ar";
        }

        this.translate.setDefaultLang(this.selected_lang);
        this.translate.use(this.selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        let filter_data:any=localStorage.getItem('filter_data');
        filter_data=JSON.parse(filter_data);

        var url = base_page.api_url() + 'api/v1/filter';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "rest_type": filter_data.rest_type,
            "rest_min_charge": filter_data.selected_min_charge,
            "selected_rating_val": filter_data.selected_rating_val,
            "selected_cities": filter_data.selected_cities,
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[(JSON.parse(data._body))];

            if (this.items[0].message == 1) {
                this.restaurants=[(this.items[0].restaurants)];
                this.restaurants_count=this.items[0].restaurants_count;
                console.log('this.restaurants_count',this.restaurants_count);
            }
        });
    }

    back() {
        this.viewCtrl.dismiss();
    }

    item(id) {

        localStorage.setItem("restaurantid", id);
        console.log(id);
        let modal = this.modalCtrl.create('ItemDetailPage');
        modal.present();
    }



}
