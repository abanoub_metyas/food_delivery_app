import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams,AlertController} from 'ionic-angular';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-forget-password',
    templateUrl: 'forget-password.html',
})
export class ForgetPasswordPage {

    public email;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public http: Http,
        public translate: TranslateService


    ) {
        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

    }

    send_old_password(){

        var url = base_page.api_url() + 'api/v1/reset_password';
        var data:any=[];
        var items:any=[];

        let postData = {
            "email": this.email,
        };

        console.log('postData',postData);

        data = this.http.post(url, postData);
        data.subscribe(data => {

            items=[(JSON.parse(data._body))];

            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans(items[0].message)
            });
            alert.present();
        });
    }

    login(){
        this.navCtrl.push('LoginPage');
    }

}
