import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, ModalController} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import {Http} from "@angular/http";
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-filter',
    templateUrl: 'filter.html',
})

export class FilterPage {
    public filter_type: any;
    public data: Observable<any>;
    public restaurant_types=[];
    public cities:any=[];
    public newData: any = [];
    public selected_types:any=[];
    public selected_cities:any="";
    public selected_min_charge:any=0;
    public max_min_charge:any=1000;
    public selected_rating_val:any=0;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        private alertCtrl: AlertController,
        protected http: Http,
        public modalCtrl: ModalController,
        public translate: TranslateService

    ) {
        this.filter_type = 1;

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        //get resturants types
        let url = base_page.api_url() + 'api/v1/get_filter_data';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.newData=[(JSON.parse(data._body))];

            console.log('this.newData',this.newData)

            if (this.newData[0].message == 1) {
                this.restaurant_types=this.newData[0].restaurant_types;
                this.max_min_charge=this.newData[0].max_minimum_charge;
                this.cities=this.newData[0].cities;
                this.selected_min_charge=this.max_min_charge;
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });

    }

    filter_tab(filter_type) {
        this.filter_type = filter_type;
    }

    updateTypes(type_id,$event){
        if($event.checked){
            this.selected_types[type_id]=type_id;
        }
        else{
            if(typeof this.selected_types[type_id]!="undefined"){
                delete this.selected_types[type_id];
            }
        }
    }


    show_result(){

        let filter_data = {
            "rest_type": this.selected_types,
            "selected_min_charge": this.selected_min_charge,
            "selected_rating_val": this.selected_rating_val,
            "selected_cities": [this.selected_cities],
        };
        console.log('filter_data',filter_data);

        localStorage.setItem('filter_data',JSON.stringify(filter_data));


        let modal = this.modalCtrl.create('FilterResultPage');
        modal.present();

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}
