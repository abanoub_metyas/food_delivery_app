import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {FilterPage} from './filter';
import {Ionic2RatingModule} from 'ionic2-rating';

@NgModule({
    declarations: [
        FilterPage,
    ],
    imports: [
        IonicPageModule.forChild(FilterPage),
        Ionic2RatingModule
    ],
})

export class FilterPageModule {
}
