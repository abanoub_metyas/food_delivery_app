import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-verify',
    templateUrl: 'verify.html',
})
export class VerifyPage {
    todo = {
        'verify_code': "",
    };
    data: Observable<any>;
    items: Observable<any>;
    selected_lang:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        protected http: Http,
        private alertCtrl: AlertController,
        public translate: TranslateService


    ) {
        this.selected_lang=localStorage.getItem("selected_lang");

        if(this.selected_lang==null){
            this.selected_lang="ar";
        }

        this.translate.setDefaultLang(this.selected_lang);
        this.translate.use(this.selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {
        console.log(localStorage.getItem("token"));
        console.log('ionViewDidEnter VerifyPage');
    }

    back() {
        this.navCtrl.pop();
    }

    resend_verification_code(){
        var res_data:any={msg:""};
        var url = base_page.api_url() + 'api/v1/resend_verification_code';

        let postData = {
            "user_id": localStorage.getItem("userid")
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {
            res_data=[JSON.parse(data._body)];
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans(res_data.msg)
            });
            alert.present();
        });
    }

    verify(){
        var res_data:any=[];
        var url = base_page.api_url() + 'api/v1/verify_account';

        let postData = {
            "user_id": localStorage.getItem("userid"),
            "verify_code": this.todo.verify_code,
        };


        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {
            res_data=[JSON.parse(data._body)];

            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans(res_data[0].msg)
            });
            alert.present();

            if (res_data[0].verified == 1) {
                this.navCtrl.push('LoginPage');
            }
        });

    }


    login() {
        this.navCtrl.push('LoginPage');
    }

}
