import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-settingsedit',
    templateUrl: 'settingsedit.html',
})

export class SettingseditPage {

    data: Observable<any>;
    data1: Observable<any>;
    newData: any = [];
    newData1: any = [];
    user: any = [];

    form: FormGroup;

    validation_messages = {
        'fname': [
            {type: 'text', message: 'Please enter your First Name'}
        ],
        'lname': [
            {type: 'text', message: 'Please enter your Last Name'}
        ],
        'phone_country_code': [
            {type: 'number', message: 'Please enter your Phone'}
        ],
        'phone': [
            {type: 'number', message: 'Please enter your Phone'}
        ],
        'address': [
            {type: 'password', message: 'Please enter your address'}
        ],
    }

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        protected http: Http,
        public formBuilder: FormBuilder,
        private alertCtrl: AlertController,
        public translate: TranslateService


    ) {
        this.form = this.formBuilder.group({
            fname: ['', Validators.required],
            lname: ['', Validators.required],
            phone_country_code: ['', Validators.required],
            phone: ['', Validators.required],
            address: ['', Validators.required],

        });

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        var url = base_page.api_url() + 'api/v1/Profile';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.newData=[(JSON.parse(data._body))];
            if (this.newData[0].message == 1) {

                this.user=[(this.newData[0].data)];
                console.log(this.user);
                this.form.get('fname').setValue(this.user[0].first_name);
                this.form.get('lname').setValue(this.user[0].last_name);
                if (this.user[0].mobile != null){
                    this.form.get('phone_country_code').setValue(this.user[0].mobile.substr(0,4));
                    this.form.get('phone').setValue(this.user[0].mobile.substr(4));
                }
                else{
                    this.form.get('phone').setValue(this.user[0].mobile);
                }
                this.form.get('address').setValue(this.user[0].address);

            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    save() {
        if (this.form.valid) {
            // Save your values, using this.form.get('myField').value;

            var url = base_page.api_url() + 'api/v1/updateProfile';

            let postData = {
                "token": localStorage.getItem("token"),
                "userid": localStorage.getItem("userid"),
                "device_id": localStorage.getItem("device_id"),
                "fname": this.form.get('fname').value,
                "lname": this.form.get('lname').value,
                "phone": this.form.get('phone').value,
                "phonekey": this.form.get('phone_country_code').value,
                "address": this.form.get('address').value,
            }

            this.data1 = this.http.post(url, postData);
            this.data1.subscribe(data1 => {

                this.newData1=[(JSON.parse(data1._body))];
                if (this.newData1[0].message == 1) {
                    this.viewCtrl.dismiss();
                }
                else {
                    console.log(this.data1);

                    let alert = this.alertCtrl.create({
                        title: this.get_trans("notification"),
                        subTitle: this.get_trans("all_fields_required")
                    });
                    alert.present();
                }

            });
        }
    }
}
