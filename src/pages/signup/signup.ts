import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page'
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {
    todo = {
        'fname': "",
        'lname': "",
        'email': "",
        'phone': "",
        'phonekey': "",
        'address': "",
        'password': "",
        'confirm_password': "",
    };
    data: Observable<any>;
    public items: any = [];
    public fname: any = [];
    public form: FormGroup;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        protected http: Http,
        public formBuilder: FormBuilder,
        private alertCtrl: AlertController,
        public translate: TranslateService


    ) {

        this.form = this.formBuilder.group({
            'email': ['', Validators.compose([Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
            'fname': ['', Validators.required],
            'lname': ['', Validators.required],
            'phone': ['', Validators.required],
            'phonekey': ['', Validators.required],
            'address': ['', Validators.required],
            'password': ['', Validators.required],
            'confirm_password': ['', Validators.required],
        });

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {
        console.log('ionViewDidEnter SignupPage');
    }

    register() {

        if (!this.form.valid) {
            console.log(this.form);
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("all_fields_required")
            });
            alert.present();

            return;
        }

        if (this.todo.password!=this.todo.confirm_password) {
            console.log(this.form);
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("password_mismatched")
            });
            alert.present();

            return;
        }



        var url = base_page.api_url() + 'api/v1/Signup';

        let postData = {
            "fname": this.todo.fname,
            "lname": this.todo.lname,
            "email": this.todo.email,
            "phone": this.todo.phone,
            "phonekey": this.todo.phonekey,
            "address": this.todo.address,
            "password": this.todo.password,
            "confirm_password": this.todo.confirm_password,
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[JSON.parse(data._body)];

            if (this.items[0].message == 1) {
                localStorage.setItem("userid", this.items[0].user_id);
                localStorage.setItem("token", this.items[0].token);
                this.navCtrl.push('VerifyPage');
            }
            else if(this.items[0].message == -1){
                console.log(this.form);
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("email_is_already_exist_you_can_not_register_with_it")
                });
                alert.present();
            }
            else {
                console.log(data);
            }
        });


    }

    login() {
        this.navCtrl.push('VerifyPage');
    }

    back() {
        this.navCtrl.pop();
    }

}
