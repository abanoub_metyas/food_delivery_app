import {Component, ViewChild, ElementRef} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';

declare var google;

@IonicPage()
@Component({
    selector: 'page-location',
    templateUrl: 'location.html',
})
export class LocationPage {

    @ViewChild('map') mapElement: ElementRef;
    map: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public viewCtrl: ViewController,
                public translate: TranslateService) {

        let selected_lang: any;
        selected_lang = localStorage.getItem("selected_lang");
        if (selected_lang == null) {
            selected_lang = "en";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword) {
        let trans_val: any = "";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val = value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {
        console.log('ionViewDidEnter LocationPage');
        this.loadMap();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    loadMap() {

        let latLng = new google.maps.LatLng(-34.9290, 138.6010);

        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    }

}
