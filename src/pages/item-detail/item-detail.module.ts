import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ItemDetailPage} from './item-detail';
import {Ionic2RatingModule} from 'ionic2-rating';

@NgModule({
    declarations: [
        ItemDetailPage,
    ],
    imports: [
        IonicPageModule.forChild(ItemDetailPage),
        Ionic2RatingModule
    ],
})
export class ItemDetailPageModule {
}
