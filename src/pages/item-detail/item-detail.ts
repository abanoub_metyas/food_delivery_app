import {Component} from '@angular/core';
import {IonicPage, NavController, ViewController, ModalController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


declare var $: any;

@IonicPage()
@Component({
    selector: 'page-item-detail',
    templateUrl: 'item-detail.html',
})

export class ItemDetailPage {
    data: Observable<any>;
    newData: any = [];
    items: any = [];
    card: any = [];
    public uploads_url:any="";
    public selected_lang:any;


    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public navParams: NavParams,
        protected http: Http,
        public modalCtrl: ModalController,
        private alertCtrl: AlertController,
        public translate: TranslateService

    ) {
        this.uploads_url=base_page.api_uploads_url();

        this.selected_lang=localStorage.getItem("selected_lang");
        if(this.selected_lang==null){
            this.selected_lang="ar";
        }

        this.translate.setDefaultLang(this.selected_lang);
        this.translate.use(this.selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }


    ionViewDidEnter() {


        if (localStorage.getItem("token") == null) {
            this.navCtrl.push('LoginPage')
        }

        var url = base_page.api_url() + 'api/v1/Restaurant';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "id": localStorage.getItem("restaurantid"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.newData=[(JSON.parse(data._body))];

            if (this.newData[0].message == 1) {
                this.items=[(this.newData[0].restaurant)];
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });
    }

    close() {
        this.viewCtrl.dismiss();
    }

    dataCart(itemCard,restaurant_id) {
        itemCard.restaurant_id=restaurant_id;
        let modal = this.modalCtrl.create('ItemDetailOnePage', itemCard);
        modal.present();
    }

    goCart(restaurant_id) {
        this.card=localStorage.getItem(restaurant_id+"_cardList");

        if(this.card==null||this.card=='[]'){
            this.card=[];
        }

        if (this.card.length > 0) {
            this.navCtrl.push('YourcartPage',{
                'restaurant_id':restaurant_id
            });
        }
        else {
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("please_add_order_to_cart")
            });
            alert.present();

        }

    }

    add_to_favorite(rest_id){
        var url = base_page.api_url() + 'api/v1/add_to_favorite';
        let ajax_data:any=[];
        let ajax_items:any=[];

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "rest_id": rest_id,
        };

        ajax_data = this.http.post(url, postData);
        ajax_data.subscribe(data => {

            ajax_items=[(JSON.parse(data._body))];

            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans(ajax_items[0].msg)
            });
            alert.present();

            $(".ion-ios-heart-outline").addClass("ion-ios-heart");
            $(".ion-ios-heart-outline").addClass("fav_rest");
            $(".ion-ios-heart-outline").removeClass("ion-ios-heart-outline");

        });
    }

}
