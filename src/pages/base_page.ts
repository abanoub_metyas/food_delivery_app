
export class base_page{

    public static api_url(){
        // return "http://localhost/tareklancer/work/food_delivery/index.php/";
        // return "http://192.168.1.3/tareklancer/work/food_delivery/index.php/";
        return "http://tareklancer.com/projects/dev_ibrahim/food_delivery/index.php/";
        // return "http://tareklancer.com/projects/abanob/hunger_station/public/index.php/";

    }

    public static api_uploads_url(){
        // return "http://localhost/tareklancer/work/food_delivery/upload/";
        // return "http://192.168.1.3/tareklancer/work/food_delivery/upload/";
        return "http://tareklancer.com/projects/dev_ibrahim/food_delivery/upload/";
        // return "http://tareklancer.com/projects/abanob/hunger_station/public/upload/";
    }

    public static convert_json_to_arr(obj){
        return Object.keys(obj).map(function(k) { return obj[k] });
    }


}
