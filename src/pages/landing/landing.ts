import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, ViewController, AlertController,Platform} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';


@IonicPage()
@Component({
    selector: 'page-landing',
    templateUrl: 'landing.html',
})
export class LandingPage {

    newData: any = [];
    items: any = [];
    cities: any = [];
    data: Observable<any>;
    selected_city:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        public translate: TranslateService,
        public alertCtrl: AlertController,
        protected http: Http,


    ) {

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        this.selected_city=localStorage.getItem("search_selected_city");
        if(this.selected_city==null){
            this.selected_city="";
        }

        let url = base_page.api_url() + 'api/v1/get_all_cities';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.newData=[(JSON.parse(data._body))];

            if (this.newData[0].message == 1) {
                this.cities=this.newData[0].cities;
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });

    }

    log() {
        if(this.selected_city==""){
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("you_should_select_your_city")
            });
            alert.present();
            return false;
        }

        localStorage.setItem("search_selected_city",this.selected_city);

        let modal = this.modalCtrl.create('SearchPage');
        modal.present();
    }

    item() {
        let modal = this.modalCtrl.create('ItemDetailPage');
        modal.present();
    }




}
