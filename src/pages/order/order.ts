import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-order',
    templateUrl: 'order.html',
})
export class OrderPage {
    data: Observable<any>;
    order_type: any;
    orders: any = [];
    order: any = [];
    pass: any = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        protected http: Http,
        private alertCtrl: AlertController,
        public translate: TranslateService

    ) {
        this.order_type = 1;

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        if (localStorage.getItem("token") == null) {
            let modal = this.modalCtrl.create('LoginPage');
            modal.present();
        }

        var url = base_page.api_url() + 'api/v1/Orders';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.orders=[(JSON.parse(data._body))];

            if (this.orders[0].message == 1) {
                this.order=[(this.orders[0].data)];
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
                console.log(data);
            }
        });
    }

    order_tab(order_type) {
        this.order_type = order_type;
    }

    orderdetail(id) {
        this.pass.push(id);
        let modal = this.modalCtrl.create('OrderdetailsPage', this.pass);
        modal.present();
    }

    sum(num1,num2){
        return parseFloat(num1)+parseFloat(num2);
    }

}
