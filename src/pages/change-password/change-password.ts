import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-change-password',
    templateUrl: 'change-password.html',
})

export class ChangePasswordPage {

    data: Observable<any>;
    data1: Observable<any>;
    newData: any = [];
    newData1: any = [];
    user: any = [];

    form: FormGroup;

    validation_messages = {
        'old_password': [
            {type: 'password', message: 'this field is required'}
        ],
        'new_password': [
            {type: 'password', message: 'this field is required'}
        ],
        'confirm_new_password': [
            {type: 'password', message: 'this field is required'}
        ],
    };

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        protected http: Http,
        public formBuilder: FormBuilder,
        private alertCtrl: AlertController,
        public translate: TranslateService


    ) {
        this.form = this.formBuilder.group({
            old_password: ['', Validators.required],
            new_password: ['', Validators.required],
            confirm_new_password: ['', Validators.required],
        });

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    save() {
        if (this.form.valid) {

            if(this.form.get('new_password').value!=this.form.get('confirm_new_password').value){

                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("password_mismatched")
                });
                alert.present();

                return;
            }

            var url = base_page.api_url() + 'api/v1/change_password';

            let postData = {
                "token": localStorage.getItem("token"),
                "userid": localStorage.getItem("userid"),
                "device_id": localStorage.getItem("device_id"),
                "old_password": this.form.get('old_password').value,
                "new_password": this.form.get('new_password').value,
            };

            this.data1 = this.http.post(url, postData);
            this.data1.subscribe(data1 => {

                this.newData1=[(JSON.parse(data1._body))];

                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans(this.newData1[0].message)
                });
                alert.present();

            });
        }
    }
}
