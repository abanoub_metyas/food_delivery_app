import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, AlertController, Events} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-ratting',
    templateUrl: 'ratting.html',
})
export class RattingPage {
    public data: Observable<any>;
    public items:any=[];
    public restaurant:any=[];
    public uploads_url=base_page.api_uploads_url();
    public rate_delivery_val:any=5;
    public rate_val:any=5;
    public review_text:any;
    public rest_id:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        protected http: Http,
        public alertCtrl:AlertController,
        public events: Events,
        public translate: TranslateService

    ) {

        events.subscribe('star-rating:changed', (starRating) => {console.log(starRating)});

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);

    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        this.rest_id=localStorage.getItem("rate_rest_id");

        var url = base_page.api_url() + 'api/v1/Restaurant';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "id": this.rest_id,
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[(JSON.parse(data._body))];
            if (this.items[0].message == 1) {
                this.restaurant=this.items[0].restaurant;
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });



    }




    submit_review() {

        var url = base_page.api_url() + 'api/v1/post_review';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "rest_id": this.rest_id,
            "rate_val": this.rate_val,
            "review_text": this.review_text,
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[(JSON.parse(data._body))];
            if (this.items[0].message == 1) {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("thank_you_for_your_rate")
                });
                alert.present();
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

            this.viewCtrl.dismiss();
        });


    }

}
