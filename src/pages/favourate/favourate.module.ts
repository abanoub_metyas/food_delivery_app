import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {FavouratePage} from './favourate';
import {Ionic2RatingModule} from 'ionic2-rating';

@NgModule({
    declarations: [
        FavouratePage,
    ],
    imports: [
        IonicPageModule.forChild(FavouratePage),
        Ionic2RatingModule

    ],
})
export class FavouratePageModule {
}
