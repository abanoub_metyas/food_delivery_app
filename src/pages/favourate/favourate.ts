import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-favourate',
    templateUrl: 'favourate.html',
})
export class FavouratePage {
    data: Observable<any>;
    public items: any = [];
    public headers: any = [];
    public restaurants: any = [];
    cart: any = [];
    public uploads_url=base_page.api_uploads_url();
    public selected_lang:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        protected http: Http,
        private alertCtrl: AlertController,
        public translate: TranslateService

    ) {
        this.selected_lang=localStorage.getItem("selected_lang");
        if(this.selected_lang==null){
            this.selected_lang="ar";
        }

        this.translate.setDefaultLang(this.selected_lang);
        this.translate.use(this.selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }


    ionViewDidEnter() {

        if (localStorage.getItem("token") == null) {
            let modal = this.modalCtrl.create('LoginPage');
            modal.present();
        }

        var url = base_page.api_url() + 'api/v1/FavourateR';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[(JSON.parse(data._body))];

            if (this.items[0].message == 1) {
                this.restaurants=[(this.items[0].restaurants)];
                console.log(this.restaurants);
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();

                console.log(data);
            }
        });

    }

    item(id) {
        localStorage.setItem("restaurantid", id);
        localStorage.setItem(id+"_cardList", JSON.stringify(this.cart));
        let modal = this.modalCtrl.create('ItemDetailPage');
        modal.present();
    }

    remove_from_favorite(rest_id){
        let alert = this.alertCtrl.create({
            title: this.get_trans("are_you_sure"),
            buttons: [
                {
                    text: this.get_trans("no"),
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {

                    text: this.get_trans("yes"),
                    handler: () => {
                        var url = base_page.api_url() + 'api/v1/remove_from_favorite';
                        let ajax_data:any=[];
                        let ajax_items:any=[];

                        let postData = {
                            "token": localStorage.getItem("token"),
                            "userid": localStorage.getItem("userid"),
                            "device_id": localStorage.getItem("device_id"),
                            "rest_id": rest_id,
                        };

                        ajax_data = this.http.post(url, postData);
                        ajax_data.subscribe(data => {

                            ajax_items=[(JSON.parse(data._body))];

                            let alert_not = this.alertCtrl.create({
                                title: this.get_trans("notification"),
                                subTitle: this.get_trans(ajax_items[0].msg)
                            });
                            alert_not.present();

                            this.ionViewDidEnter();
                        });
                    }
                }
            ]
        });

        alert.present();




    }
}
