import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, ViewController} from 'ionic-angular';
import {base_page} from '../base_page';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-item-detail-one',
    templateUrl: 'item-detail-one.html',
})
export class ItemDetailOnePage {

    item: any;
    cuantity: any;
    card: any = [];
    cardItem: any = [];
    public uploads_url=base_page.api_uploads_url();
    public restaurant_id;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        public translate: TranslateService

    ) {
        this.item = this.navParams.data;
        this.restaurant_id=this.item.restaurant_id;
        this.card = JSON.parse(localStorage.getItem(this.restaurant_id+"_cardList"));

        if(this.card==null||this.card=='[]'){
            this.card={};
        }

        this.cuantity = 1;

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        console.log('ionViewDidEnter ItemDetailOnePage');
    }

    increment() {
        this.cuantity += 1;
    }

    decrement() {
        if (this.cuantity > 1) {
            this.cuantity -= 1;
        }

    }

    back() {
        this.navCtrl.pop();

    }

    cart() {
        this.cardItem = [this.item.item_id, this.cuantity];

        if(typeof (this.card[this.item.item_id])!="undefined"){
            delete this.card[this.item.item_id];
        }

        this.card[this.item.item_id]=this.cardItem;

        localStorage.setItem(this.restaurant_id+"_cardList",JSON.stringify(this.card));
        this.viewCtrl.dismiss(this.card);
    }

}
