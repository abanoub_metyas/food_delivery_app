import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, ViewController, AlertController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page'
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-search',
    templateUrl: 'search.html',
})

export class SearchPage {
    data: Observable<any>;
    public items: any = [];
    public headers: any = [];
    public restaurants: any = [];
    public uploads_url=base_page.api_uploads_url();
    public selected_lang:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController,
        protected http: Http,
        public alertCtrl:AlertController,
        public translate: TranslateService

    ) {
        this.selected_lang=localStorage.getItem("selected_lang");
        if(this.selected_lang==null){
            this.selected_lang="ar";
        }

        this.translate.setDefaultLang(this.selected_lang);
        this.translate.use(this.selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {
        console.log('ionViewDidEnter SearchPage');

        var url = base_page.api_url() + 'api/v1/Home';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "search_selected_city": localStorage.getItem("search_selected_city"),
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[(JSON.parse(data._body))];

            if (this.items[0].message == 1) {
                this.restaurants=[(this.items[0].restaurants)];
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }
        });
    }

    back() {
        this.viewCtrl.dismiss();
    }

    filter() {
        let modal = this.modalCtrl.create('FilterPage');
        modal.present();
    }

    item(id) {

        localStorage.setItem("restaurantid", id);
        console.log(id);
        let modal = this.modalCtrl.create('ItemDetailPage');
        modal.present();
    }



}
