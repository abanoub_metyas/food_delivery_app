import {Component, ViewChild, ElementRef} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation';
import {IonicPage,ViewController , NavController, NavParams, ModalController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


declare var google;
declare var $: any;

@IonicPage()
@Component({
    selector: 'page-delivery',
    templateUrl: 'delivery.html',
})

export class DeliveryPage {

    @ViewChild('map') mapElement: ElementRef;

    data: Observable<any>;
    map: any;
    allItems: any = [];
    pass: any = [];
    myLat: any;
    myLong: any;
    address: any;
    currentLocatione: any;
    toggleStatus: any = false;
    restaurant_id: any;
    click_at_order_btn:boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        private geolocation: Geolocation,
        protected http: Http,
        private alertCtrl: AlertController,
        public viewCtrl: ViewController,
        public translate: TranslateService

    ) {
        this.restaurant_id = this.navParams.get("restaurant_id");

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }


    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        this.click_at_order_btn=false;

        let ajax_data:any=[];
        var url = base_page.api_url() + 'api/v1/Profile';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            ajax_data=[(JSON.parse(data._body))];
            if (ajax_data[0].message == 1) {
                ajax_data=[(ajax_data[0].data)];

                console.log('ajax_data',ajax_data,ajax_data[0].address);

                this.address=ajax_data[0].address;
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });


        if (this.address == null || this.address == "" || this.address == undefined) {
            this.address = this.get_trans("no_address_written");
        }

        this.geolocation.getCurrentPosition().then(pos => {

            this.myLat = pos.coords.latitude;
            this.myLat = pos.coords.longitude;

            localStorage.setItem("lat", this.myLat);
            localStorage.setItem("lang", this.myLat);

            this.loadMap(pos.coords.latitude, pos.coords.longitude);
            this.getAddress(pos.coords.latitude, pos.coords.longitude);

        });
    }

    getAddress(myLat, myLong) {

        let geocoder = new google.maps.Geocoder;
        let latlng = {lat: myLat, lng: myLong};
        geocoder.geocode({'location': latlng}, (results, status) => {

            localStorage.setItem("locat", results[0].formatted_address);
            this.currentLocatione = localStorage.getItem('locat');
        });

    }

    loadMap(myLat, myLong) {

        let latLng = new google.maps.LatLng(myLat, myLong);

        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }


    editLocat() {
        if (this.toggleStatus == true) {
            this.toggleStatus = false;
            console.log(this.toggleStatus);
        }
        else {
            this.toggleStatus = true;
            console.log(this.toggleStatus);
        }
    }

    back() {
        this.navCtrl.pop();
    }

    order() {

        if (this.click_at_order_btn){
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("please_wait")
            });
            alert.present();
            return false;
        }

        this.click_at_order_btn=true;

        let check_card_items:any=localStorage.getItem(this.restaurant_id+"_cardList");
        if(check_card_items==null){
            check_card_items=[];
        }

        if (check_card_items.length == 0) {
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("please_add_order_to_cart")
            });
            alert.present();
            return;
        }


        var url = base_page.api_url() + 'api/v1/addCart';
        let lat:any;
        let lang:any;

        if(!this.toggleStatus){
            this.address=this.currentLocatione;
            lat=localStorage.getItem("lat");
            lang=localStorage.getItem("lang");
        }
        else{
            lat="";
            lang="";
        }

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "restaurant": this.restaurant_id,
            "items": localStorage.getItem(this.restaurant_id+"_cardList"),
            "lat": lat,
            "lang": lang,
            "address":this.address

        };
        let alert;

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {
            localStorage.removeItem(this.restaurant_id+"_cardList");

            this.allItems=[(JSON.parse(data._body))];

            if (this.allItems[0].message != 0&&this.allItems[0].message != 1) {
                alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans(this.allItems[0].message)
                });
                alert.present();
            }
            else{

                if (this.allItems[0].message == 1) {
                    this.pass.push(this.allItems[0].order_id);
                    this.navCtrl.push('OrderdetailsPage',this.pass);
                }
                else {
                    alert = this.alertCtrl.create({
                        title: this.get_trans("notification"),
                        subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                    });
                    alert.present();
                }
            }

        });


    }

}
