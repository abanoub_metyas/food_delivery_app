import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplainsAndSuggestionsPage } from './complains-and-suggestions';

@NgModule({
  declarations: [
    ComplainsAndSuggestionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplainsAndSuggestionsPage),
  ],
})
export class ComplainsAndSuggestionsPageModule {}
