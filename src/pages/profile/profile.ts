import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    data: Observable<any>;
    newData: any = [];
    user: any = [];

    imageURI: any;
    imageFileName: any = 'assets/imgs/item10.png';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        protected http: Http,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        private alertCtrl: AlertController,
        public translate: TranslateService


    ) {
        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {

        var url = base_page.api_url() + 'api/v1/Profile';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.newData=[(JSON.parse(data._body))];
            if (this.newData[0].message == 1) {

                this.user=[(this.newData[0].data)];

            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });

    }


    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }

    settings() {
        let modal = this.modalCtrl.create('SettingsPage');
        modal.present();
    }

    edit() {
        let modal = this.modalCtrl.create('SettingseditPage');
        modal.present();
    }

    open_technical_support_page() {
        let modal = this.modalCtrl.create('TechnicalSupportPage');
        modal.present();
    }

    open_complains_and_suggestions_page() {
        let modal = this.modalCtrl.create('ComplainsAndSuggestionsPage');
        modal.present();
    }

    open_change_password_page() {
        let modal = this.modalCtrl.create('ChangePasswordPage');
        modal.present();
    }

    change_lang(lang){
        localStorage.setItem("selected_lang",lang);

        this.translate.setDefaultLang(lang);
        this.translate.use(lang);
    }

}
